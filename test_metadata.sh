#!/bin/bash
#   ============================================================================
#   @file   test_metadata.sh
#
#   @desc   Script to test metadata file
#
#   ============================================================================
#   Revision History
#   22-08-2020 Sivaraj         Initial draft.
#
#   ============================================================================
#Usage
# ./test_metadata.sh --soc="j721e"

#Get user input
while [ $# -gt 0 ]; do
    case "$1" in
        --soc=*)
            soc="${1#*=}"
            ;;
        -h|--help)
            echo " "
            echo "Usage: $0 --soc=<>"
            echo "Optional arguments:"
            echo "      --soc=<j721e/j7200/am65xx>"
            echo " "
            exit 0
            ;;
        *)
            printf "Error: Invalid argument $1!!\n"
    esac
    shift
done

#Default value if not provided
working_dir=`pwd`
: ${soc:="j721e"}
quiet_mode_flag="-q"

source ${soc}/metadata.inc

firmare_filename="coresdk_rtos_${CORESDK_RTOS_SOC}_${CORESDK_RTOS_VERSION}_firmware.tar.xz"
linux_filename="coresdk_rtos_${CORESDK_RTOS_FAMILY}_${CORESDK_RTOS_VERSION}_linux.tar.xz"
windows_filename="coresdk_rtos_${CORESDK_RTOS_FAMILY}_${CORESDK_RTOS_VERSION}_windows.tar.xz"
pdk_filename="pdk_${CORESDK_RTOS_FAMILY}_${CORESDK_RTOS_VERSION}.zip"

get_sha256sum() {
    local sha256sum=`sha256sum -b ${1} | cut -d " " -f 1`
    echo ${sha256sum}
}

verify_sha256 () {
    local filename=$1
    local url=$2
    local expected_sha=$3

    rm -rf ${filename}
    #Get file
    echo "[${filename}] Downloading from ${url} ..."
    wget $quiet_mode_flag ${url}

    #Calculate SHA256
    local sha256sum=$(get_sha256sum ${filename})

    if [ "${expected_sha}" == "${sha256sum}" ]; then
        echo "[${filename}] SHA256 ${sha256sum} matches!!"
        echo " "
    else
        echo "[${filename}] Error: SHA256 ${sha256sum} doesn't match with expected SHA256 ${expected_sha}!!"
        echo " "
    fi

    rm -rf ${filename}
}

#Firmware applicable only for these devices
if [ "${soc}" == "j721e" ] || [ "${soc}" == "j7200" ] || [ "${soc}" == "am65xx" ] ; then
    verify_sha256 ${firmare_filename} ${CORESDK_RTOS_FIRMWARE_URL} ${CORESDK_RTOS_FIRMWARE_SHA256SUM}
fi
verify_sha256 ${pdk_filename}       ${PDK_URL}                      ${PDK_SHA256SUM}
verify_sha256 ${linux_filename}     ${CORESDK_RTOS_LINUX_URL}       ${CORESDK_RTOS_LINUX_SHA256SUM}
verify_sha256 ${windows_filename}   ${CORESDK_RTOS_WINDOWS_URL}     ${CORESDK_RTOS_WINDOWS_SHA256SUM}
