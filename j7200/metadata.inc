#COMMON
CORESDK_RTOS_VERSION="08_00_00_12"
CORESDK_RTOS_VERSION_DOT="08.00.00.12"
CORESDK_RTOS_VERSION_SHORT="08_00_00"
CORESDK_RTOS_VERSION_SHORT_DOT="08.00.00"
CORESDK_RTOS_FAMILY="j7200"
CORESDK_RTOS_SOC="j7200"
CORESDK_RTOS_SOC_CAPS="J7200"
CORESDK_RTOS_WEBLINK_NIGHTLY="http://gtweb.dal.design.ti.com/nightly_builds/core-sdk-rtos-${CORESDK_RTOS_FAMILY}/448-2021-06-08_07-11-54/artifacts/output/webgen/exports"
CORESDK_RTOS_WEBLINK_RC="http://tigt_qa.dal.englab.ti.com/qacm/test_area/CORESDK/08.00.00/rtos/08_00_00_12/${CORESDK_RTOS_FAMILY}/08_00_00_12/exports"
CORESDK_RTOS_WEBLINK_EXTERNAL="https://software-dl.ti.com/jacinto7/esd/processor-sdk-rtos-j7200/firmware/08_00_00_12"
CORESDK_RTOS_WEBLINK="${CORESDK_RTOS_WEBLINK_RC}"
CORESDK_RTOS_WEBLINK_FIRMWARE="${CORESDK_RTOS_WEBLINK_EXTERNAL}"
CORESDK_RTOS_MD5SUM="${CORESDK_RTOS_WEBLINK}/md5sum.txt"

#Tools and Dependencies
BIOS_VERSION="6_83_02_07"
XDC_VERSION="3_61_04_40_core"
NDK_VERSION="3_80_00_19"
NS_VERSION="2_80_00_17"
CGT_ARM_VERSION="20.2.0.LTS"
CGT_C6X_VERSION="8.3.7"
CGT_C7X_VERSION="1.4.2.LTS"
CGT_PRU_VERSION="2.3.3"
GCC_ARCH64_VERSION="9.2-2019.12"
GCC_ARCH64_VERSION_WEB="9.2019.12"
GCC_ARCH64_BIN_PREFIX_STR="aarch64-elf"
CG_XML_VERSION="2.61.00"
UIA_VERSION="2_30_01_02"
XDAIS_VERSION="7_24_00_04"
DSPLIB_C66_VERSION="3_4_0_0"
MATHLIB_C66_VERSION="3_1_2_1"

#CORESDK RTOS Linux
CORESDK_RTOS_LINUX_VERSION="${CORESDK_RTOS_VERSION_DOT}"
CORESDK_RTOS_LINUX_URL="${CORESDK_RTOS_WEBLINK}/coresdk_rtos_${CORESDK_RTOS_FAMILY}_${CORESDK_RTOS_VERSION}_linux.tar.xz"
CORESDK_RTOS_LINUX_SHA256SUM="53e2b9fb9baed390e369a68db9fa5b775b59ad4c41d469ad684ba9cf0179583b"

#CORESDK RTOS Windows
CORESDK_RTOS_WINDOWS_VERSION="${CORESDK_RTOS_VERSION_DOT}"
CORESDK_RTOS_WINDOWS_URL="${CORESDK_RTOS_WEBLINK}/coresdk_rtos_${CORESDK_RTOS_FAMILY}_${CORESDK_RTOS_VERSION}_windows.tar.xz"
CORESDK_RTOS_WINDOWS_SHA256SUM="73d131767dd37dd3ba325b255a38e6eb2af5bb043a58bebb04d2244b830f0841"

#CORESDK RTOS Firmwares
CORESDK_RTOS_FIRMWARE_VERSION="${CORESDK_RTOS_VERSION_DOT}"
CORESDK_RTOS_FIRMWARE_URL="${CORESDK_RTOS_WEBLINK_FIRMWARE}/coresdk_rtos_${CORESDK_RTOS_SOC}_${CORESDK_RTOS_VERSION}_firmware.tar.xz"
CORESDK_RTOS_FIRMWARE_SHA256SUM="a300848a08543215f8804066e3b5b8fdd61be30e95efdb3d54309cc1b0da8035"

#CORESDK RTOS Addon
CORESDK_RTOS_ADDON_VERSION="${CORESDK_RTOS_VERSION_DOT}"
CORESDK_RTOS_ADDON_URL="${CORESDK_RTOS_WEBLINK}/coresdk_rtos_${CORESDK_RTOS_FAMILY}_${CORESDK_RTOS_VERSION}_addon.tar.xz"
CORESDK_RTOS_ADDON_SHA256SUM="3e4804fc269aa892dad68fb38afdf0b1c4cc7827d5ee006e202f3feb78b78c97"

#CORESDK RTOS Docs
CORESDK_RTOS_DOCS_VERSION="${CORESDK_RTOS_VERSION_DOT}"
CORESDK_RTOS_DOCS_URL="${CORESDK_RTOS_WEBLINK}/coresdk_rtos_${CORESDK_RTOS_FAMILY}_${CORESDK_RTOS_VERSION}_docs_only.tar.xz"
CORESDK_RTOS_DOCS_SHA256SUM="527ca7e783bc8514f3dd45b081910d52f2d4c217835e796ba078672e2eccea28"

#PDK
PDK_VERSION="${CORESDK_RTOS_VERSION_DOT}"
PDK_URL="${CORESDK_RTOS_WEBLINK}/pdk_${CORESDK_RTOS_FAMILY}_${CORESDK_RTOS_VERSION}.zip"
PDK_SHA256SUM="80957027e3e05bf4764685ad09bc790f34b944ca4bd6bfa4185d213d390a5734"

#ETHFW
ETHFW_VERSION="${CORESDK_RTOS_VERSION_DOT}"
ETHFW_URL="${CORESDK_RTOS_WEBLINK}/ethfw.zip"
ETHFW_SHA256SUM="ae927f00a635723edc4fd5f6d253d0b620efe347e00b6f8d58deee5d7a2868f6"

#MCUSW
MCUSW_VERSION="${CORESDK_RTOS_VERSION_DOT}"
MCUSW_URL="${CORESDK_RTOS_WEBLINK}/mcusw.zip"
MCUSW_SHA256SUM="93d2a82e7ce11952953ed5d4d5713d709b376ab9a8865dbd6b712fd3ca0316df"

#REMOTE DEVICE
REMOTE_DEVICE_VERSION="${CORESDK_RTOS_VERSION_DOT}"
REMOTE_DEVICE_URL="${CORESDK_RTOS_WEBLINK}/remote_device.zip"
REMOTE_DEVICE_SHA256SUM="537f2a16236e3309c94247bc546b355326fdf4aaaaacfbda34e22c7799c8a291"

