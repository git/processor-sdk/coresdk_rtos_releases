#COMMON
CORESDK_RTOS_VERSION="08_00_00_08"
CORESDK_RTOS_VERSION_DOT="08.00.00.08"
CORESDK_RTOS_VERSION_SHORT="08_00_00"
CORESDK_RTOS_VERSION_SHORT_DOT="08.00.00"
CORESDK_RTOS_FAMILY="am64x"
CORESDK_RTOS_SOC="am64x"
CORESDK_RTOS_SOC_CAPS="AM64X"
CORESDK_RTOS_WEBLINK_NIGHTLY="http://gtweb.dal.design.ti.com/nightly_builds/mcu_plus_sdk_am64x/517-2021-06-08_11-04-05/artifacts/output/webgen/publish/MCU-PLUS-SDK-AM64X/08_00_00_08/exports"
CORESDK_RTOS_WEBLINK_RC="http://tigt_qa.dal.englab.ti.com/qacm/test_area/MCU_PLUS_SDK/08.00.00/am64x/08_00_00_08/exports"
CORESDK_RTOS_WEBLINK_EXTERNAL="https://software-dl.ti.com/processor-sdk-rtos/esd/AM64X/firmware/08_00_00_08"
CORESDK_RTOS_WEBLINK="${CORESDK_RTOS_WEBLINK_NIGHTLY}"
CORESDK_RTOS_WEBLINK_FIRMWARE="${CORESDK_RTOS_WEBLINK_NIGHTLY}"
CORESDK_RTOS_MD5SUM="${CORESDK_RTOS_WEBLINK}/md5sum.txt"

#Tools and Dependencies
CCS_VERSION="10.3.1.00003"
CGT_TI_ARM_CLANG_VERSION="1.3.0"
CGT_TI_ARM_CLANG_VERSION_SUFFIX="LTS"
NODEJS_VERSION="12.18.4"
SYSCFG_VERSION="1.8.1_1900"
DOXYGEN_VERSION="1.8.20"

#SDK Linux
CORESDK_RTOS_LINUX_VERSION="08.00.00.08"
CORESDK_RTOS_LINUX_URL="${CORESDK_RTOS_WEBLINK}/mcu_plus_sdk_am64x_08_00_00_08-linux-x64-installer.run"
CORESDK_RTOS_LINUX_SHA256SUM="94b33a286a1f10aaf6744a527c8980899aa3cb4ff37860fe84edc56df0b12162"

#SDK Windows
CORESDK_RTOS_WINDOWS_VERSION="08.00.00.08"
CORESDK_RTOS_WINDOWS_URL="${CORESDK_RTOS_WEBLINK}/mcu_plus_sdk_am64x_08_00_00_08-windows-x64-installer.exe"
CORESDK_RTOS_WINDOWS_SHA256SUM="681a032920ec5df79f42ac2cb43564af1ff5b5778b87fecaa2488f3415ff7eff"

#SDK Firmwares
CORESDK_RTOS_FIRMWARE_VERSION="08.00.00.08"
CORESDK_RTOS_FIRMWARE_URL="${CORESDK_RTOS_WEBLINK_FIRMWARE}/mcu_plus_sdk_am64x_08_00_00_08_firmware.tar.xz"
CORESDK_RTOS_FIRMWARE_SHA256SUM="497b2a39e397aaf825d652301516d8122e9cdfa95aeacb35952f2d0c5b6db99d"

#SDK Demos Firmwares
CORESDK_RTOS_FIRMWARE_DEMOS_VERSION="08.00.00.08"
CORESDK_RTOS_FIRMWARE_DEMOS_URL="${CORESDK_RTOS_WEBLINK_FIRMWARE}/mcu_plus_sdk_am64x_08_00_00_08_firmware_demos.tar.xz"
CORESDK_RTOS_FIRMWARE_DEMOS_SHA256SUM="2f905213f6172248988b43a776a1ce6276bebeaf351f261031c39c0e30c1c92b"

#SDK Docs
CORESDK_RTOS_DOCS_VERSION="08.00.00.08"
CORESDK_RTOS_DOCS_URL="${CORESDK_RTOS_WEBLINK}/mcu_plus_sdk_am64x_08_00_00_08_docs_only.tar.xz"
CORESDK_RTOS_DOCS_SHA256SUM="10154c1d132e82cb9916888cb8ad919875e1d6d3bedcf1f8a05256363fa07956"

#SDK TIREX
CORESDK_RTOS_TIREX_VERSION="08.00.00.08"
CORESDK_RTOS_TIREX_URL="${CORESDK_RTOS_WEBLINK}/mcu_plus_sdk_am64x_08_00_00_08__all.zip"
CORESDK_RTOS_TIREX_SHA256SUM="09174ed75adcd64289f8da996dddc1c6a28d074585568814fb213921b8a6fefb"

